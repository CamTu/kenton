var app = app || {};

app.init = function() {
  app.anchor();
  app.toTop();
  app.keyVisua();
  app.sliderSpecial();
  app.sliderModel();
  app.scrollHeader();
  app.fancyBoxTop();

};


app.animation = function() {
  var wow = new WOW({
    boxClass: 'wow',
    animateClass: 'animated',
    offset: 0,
    mobile: false,
    live: true
  });
  wow.init();
};
app.keyVisua = function() {
  $('.key-visua').slick({
    dots: false,
    infinite: true,
  });
};

app.anchor = function() {
  // anchor

  $('.anchor').click(function() {
    $header_height = $('header').outerHeight();

    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top - 80
        }, 1000);
        return false;
      }
    }
  });
};

app.toTop = function() {
  var el = $('.btn-to-top');
  el.click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 500);
  });
}


app.scrollHeader = function() {
  if ($(window).width() > 768) {
    $(window).on('load scroll resize', function() {
      if ($(window).scrollTop() > 200) {
        $('.header-logo img').css({
          width: "160px"
        });
      }
      if ($(window).scrollTop() <= 200) {
        $('.header-logo img').css({
          width: "auto"
        });
      }
    });
  }
}

app.sliderSpecial = function() {
  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  });
  $('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: true,
    focusOnSelect: true
  });
}
app.sliderModel = function() {
  $('.slider-model .slider-main').slick({
    centerMode: true,
    infinite: true,
    centerPadding: '60px',
    slidesToShow: 1,
    speed: 500,
    variableWidth: false,
    dots: true,
    responsive: [{
        breakpoint: 992,
        settings: {
          arrows: true,
          centerMode: false,
          centerPadding: '40px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: true,
          centerMode: false,
          centerPadding: '0',
          slidesToShow: 1
        }
      }
    ]
  });
}

function myMap() {
  var myCenter = new google.maps.LatLng(10.727891, 106.7432861);
  var map = new google.maps.Map(document.getElementById('maps'), {
    zoom: 12,
    center: myCenter
  });
  var marker = new google.maps.Marker({
    position: myCenter,
    map: map

  });

}

app.fancyBoxTop = function() {
  $('[data-fancybox]').fancybox({
    toolbar: false,
    afterLoad: function(instance, current) {
      current.$content.append('<div class="btn-close" data-fancybox-close></div>');
      current.$content.append('<div class="theater-overlay"><div class="ov-before"></div><div class="ov-after"></div></div>');
      setTimeout(function() {
        $('.theater-overlay .ov-before').addClass('full');
      }, 500);
      setTimeout(function() {
        $('.theater-overlay .ov-after').addClass('full');
      }, 2000);
      setTimeout(function() {
        var iframeSrc = current.$content.find('iframe').attr('src');
        iframeSrc = iframeSrc.replace('autoplay=0', 'autoplay=1');
        current.$content.find('iframe').attr('src', iframeSrc);
      }, 3000);
      setTimeout(function() {
        current.$content.find('iframe').css('opacity', 1);
        current.$content.find('.btn-close').css('opacity', 1);
      }, 4000);

      current.$content.css({
        overflow: 'visible',
        background: '#000'
      });
    }
  });
}




$(function() {
  app.init();
});
